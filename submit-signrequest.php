<?php
require_once __DIR__ . '/vendor/autoload.php';

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();

$redirectUrl = $_ENV['REDIRECT_URL'];
$callbackUrl = $_ENV['CALLBACK_URL'];

$tokens = new \Example\TokenRepository($_ENV['PAT']);
$api = \Stiply\Api::create($tokens);

$signRequest = $api->createSignRequest([
    'file' => \file_get_contents(__DIR__.'/Contract-Tags.pdf'),
    'filename' => 'contract-tags.pdf',
    'term' => '2w',
    'call_back_url' => $callbackUrl
]);

print_r($signRequest);
$key = $signRequest->data["sign_request"]["key"];

$signer = $api->createSigner($key, [
    'signer_email' => $_ENV['SIGNER_EMAIL'],
    // 'invitation_method' => 'custom', // add invitation_method custom when Stiply should not send an invation email
    'redirect_url' => $redirectUrl,
    'signer_signature_fields' => [
        ["name" => "signature_0"]
    ],
    'signer_initial_fields' => [
        ["name" => "initial_0_required"],
        ["name" => "initial_0", "optional" => 1],
        ["name" => "initial_0_optional"]
    ],
    'signer_checkbox_fields'=>[
        ["name" => "c_0"]
    ],
    'signer_date_fields'=>[
        ["name" => "date_0"]
    ],
    'signer_text_fields'=>[
        ["name" => "text_0"]
    ]
]);

print_r($signer);

$result = $api->sendSignRequest($key);
print_r($result);

$signUrl = $result->data["sign_request"]["signers"][0]["sign_url"]; // when invation_method = custom the sign_url is provided for the signer
echo ($signUrl);